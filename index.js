var express = require('express');
let Parser = require('rss-parser');
let parser = new Parser();
var path = require('path');

var app=express();


const port = process.env.PORT || 3000;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb',extended: true}));

app.get('/', (req, res) => {
  (async () => {
    res.render('reader',{});
  })();
});

app.post('/', (req, res) => {
  (async () => {
    let feed = await parser.parseURL(req.body.url);
    res.render('reader',feed);
  })();
});

app.listen(port, () => {
  console.log(`listening on port ${ port }`);
});


